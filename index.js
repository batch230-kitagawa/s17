console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printInfo(){
	let fullName = prompt("Hi! Please add your full name.");
	let age = prompt("Hi! Please add your age.");
	let location =  prompt("Hi! Please add your location.");

	console.log("Hello " + fullName);
	console.log("You are " + age + " years old");
	console.log("You live in " + location );
	}

	printInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function showBands(){

		console.log("1. One OK Rock");
		console.log("2. Ben & Ben");
		console.log("3. One Direction");
		console.log("4. BTS");
		console.log("5. Eraserheads");
	}

	showBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showMovies(){
		console.log("1. Avatar");
		console.log("Rotten Tomatoes Rating: 77%");
		console.log("2. One Piece Film Red");
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("3. Suzume");
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("4. Minions: The Rise of Gru");
		console.log("Rotten Tomatoes Rating: 70%");
		console.log("5. Harry Potter 20th Anniversary: Return to Hogwart");
		console.log("Rotten Tomatoes Rating: 93%");
	}

	showMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();
//let printFriends() = function printUsers(){
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	//let friend1 = alert("Enter your first friend's name:"); 
	let friend1 = prompt("Enter your first friend's name:"); 
	//let friend2 = prom("Enter your second friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	//console.log(friends);
	console.log(friend3); 
};

printUsers();
//console.log(friend1);
//console.log(friend2);